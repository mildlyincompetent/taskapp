{-# LANGUAGE OverloadedStrings #-}

module TaskIO
  ( createTaskList
  , saveTaskList
  , readTaskList
  , execTaskOp
  , listTaskNames
  ) where

import           Data.Aeson           (decodeStrict, encode)
import           Data.ByteString      (readFile)
import           Data.ByteString.Lazy (writeFile)
import           Prelude              hiding (readFile, writeFile)
import           System.Directory     (listDirectory)

import           Task                 (Task (..), TaskList)

createTaskList :: String -> IO ()
createTaskList name = saveTaskList name []

saveTaskList :: String -> TaskList -> IO ()
saveTaskList name tl = writeFile fileName listBytes
  where
    fileName = name ++ ".tl"
    listBytes = encode tl

readTaskList :: String -> IO TaskList
readTaskList name = do
  inBytes <- readFile fileName
  let tl =
        case decodeStrict inBytes of
          Nothing       -> []
          Just taskList -> taskList
  return tl
  where
    fileName = name ++ ".tl"

execTaskOp :: String -> (a -> TaskList -> TaskList) -> a -> IO ()
execTaskOp name f a = do
  inBytes <- readFile fileName
  let outBytes =
        case decodeStrict inBytes of
          Nothing -> ""
          Just tl -> encode $ f a tl
  writeFile fileName outBytes
  where
    fileName = name ++ ".tl"

listTaskNames :: IO [String]
listTaskNames = do
  fileNames <- listDirectory "."
  return $ map removeExtension $ filter ending fileNames
  where
    ending ".tl"  = True
    ending (x:xs) = ending xs
    ending _      = False
    removeExtension (x:".tl") = [x]
    removeExtension (x:xs)    = x : removeExtension xs
    removeExtension []        = []
