{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module TaskAPI
  ( serverApp
  , execServer
  ) where

import           Control.Monad.IO.Class   (liftIO)
import           Network.Wai.Handler.Warp (run)
import           Servant
import           Servant.API
import           Servant.Server
import           Task                     (Task (..), TaskList, addTask,
                                           completeTask, delTask, modifyTask)
import           TaskIO                   (createTaskList, execTaskOp,
                                           listTaskNames, readTaskList,
                                           saveTaskList)

type TaskListAPI = "tasks" :> Get '[ JSON] [FilePath]

type CreateTaskAPI
   = "tasks" :> Capture "name" FilePath :> "create" :> PostNoContent '[ JSON] NoContent

type WriteTaskAPI
   = "tasks" :> Capture "name" FilePath :> ReqBody '[ JSON] TaskList :> PutNoContent '[ JSON] NoContent

type ReadTaskAPI
   = "tasks" :> Capture "name" FilePath :> "read" :> Get '[ JSON] TaskList

type AddItemAPI
   = "tasks" :> Capture "name" FilePath :> "add" :> ReqBody '[ JSON] Task :> Post '[ JSON] Bool

type CompleteItemAPI
   = "tasks" :> Capture "name" FilePath :> "items" :> Capture "taskId" String :> "complete" :> PostNoContent '[ JSON] NoContent

type DeleteItemAPI
   = "tasks" :> Capture "name" FilePath :> "items" :> Capture "taskId" String :> DeleteNoContent '[ JSON] NoContent

type ModifyItemAPI
   = "tasks" :> Capture "name" FilePath :> "items" :> Capture "taskId" String :> ReqBody '[ JSON] String :> PutNoContent '[ JSON] NoContent

type ServerAPI
   = TaskListAPI :<|> CreateTaskAPI :<|> WriteTaskAPI :<|> ReadTaskAPI :<|> AddItemAPI :<|> CompleteItemAPI :<|> DeleteItemAPI :<|> ModifyItemAPI

apiServer :: Server ServerAPI
apiServer =
  taskListAPI :<|> createTaskAPI :<|> writeTaskAPI :<|> readTaskAPI :<|>
  addItemAPI :<|>
  completeItemAPI :<|>
  deleteItemAPI :<|>
  modifyItemAPI
  where
    taskListAPI :: Handler [FilePath]
    taskListAPI = liftIO listTaskNames
    createTaskAPI :: FilePath -> Handler NoContent
    createTaskAPI fp = do
      liftIO (createTaskList fp)
      return NoContent
    writeTaskAPI :: FilePath -> TaskList -> Handler NoContent
    writeTaskAPI fp tl = do
      liftIO $ saveTaskList fp tl
      return NoContent
    readTaskAPI :: FilePath -> Handler TaskList
    readTaskAPI fp = liftIO $ readTaskList fp
    addItemAPI :: FilePath -> Task -> Handler Bool
    addItemAPI fp t = do
      liftIO $ execTaskOp fp addTask t
      return True
    completeItemAPI :: FilePath -> String -> Handler NoContent
    completeItemAPI fp t = do
      liftIO $ execTaskOp fp completeTask t
      return NoContent
    deleteItemAPI :: FilePath -> String -> Handler NoContent
    deleteItemAPI fp t = do
      liftIO $ execTaskOp fp delTask t
      return NoContent
    modifyItemAPI :: FilePath -> String -> String -> Handler NoContent
    modifyItemAPI fp t c = do
      liftIO $ execTaskOp fp (modifyTask t) c
      return NoContent

serverAPI :: Proxy ServerAPI
serverAPI = Proxy

serverApp :: Application
serverApp = serve serverAPI apiServer

execServer :: IO ()
execServer = run 8081 serverApp
