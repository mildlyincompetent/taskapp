{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Task
  ( Task(..)
  , TaskList
  , addTask
  , liftTaskOp
  , completeTask
  , delTask
  , modifyTask
  ) where

import           Data.Aeson (FromJSON (..), ToJSON (..), object, pairs,
                             withObject, (.:), (.=))

data Task =
  Task
    { taskId      :: String
    , taskDone    :: Bool
    , taskContent :: String
    }
  deriving (Show)

instance FromJSON Task where
  parseJSON =
    withObject "Task" $ \o ->
      Task <$> o .: "taskId" <*> o .: "taskDone" <*> o .: "taskContent"

instance ToJSON Task where
  toJSON Task {..} =
    object
      ["taskId" .= taskId, "taskDone" .= taskDone, "taskContent" .= taskContent]
  toEncoding Task {..} =
    pairs
      ("taskId" .= taskId <>
       "taskDone" .= taskDone <> "taskContent" .= taskContent)

type TaskList = [Task]

addTask :: Task -> TaskList -> TaskList
addTask = (:)

liftTaskOp :: (Task -> Maybe Task) -> String -> TaskList -> TaskList
liftTaskOp to id [] = []
liftTaskOp to id (t@Task {..}:ts) =
  if taskId == id
    then case to t of
           Just t' -> t' : ts
           Nothing -> ts
    else t : liftTaskOp to id ts

completeTask :: String -> TaskList -> TaskList
completeTask = liftTaskOp (\t -> Just t {taskDone = True})

delTask :: String -> TaskList -> TaskList
delTask = liftTaskOp (\_ -> Nothing)

modifyTask :: String -> String -> TaskList -> TaskList
modifyTask c = liftTaskOp (\t -> Just t {taskContent = c})
